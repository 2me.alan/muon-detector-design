import csv
import os
import numpy as np
import matplotlib.pyplot as plt

path = os.getcwd()
files = []
datasets = []

for file in os.listdir(os.path.join(path, "Originals")):
    if os.path.splitext(file)[-1].lower() == ".csv":
        files.append(file)


overallMax = 0
overallMin = 0
overallAverage = 0

for file in files:
    times = []
    values = []
    combo = []
    filepath = os.path.join(path, "Originals", file)
    with open(filepath) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            times.append(row[0])
            values.append(row[1])
            combo.append([row[0], row[1]])
        times = np.array(times).astype(np.float32)
        values =  np.array(values).astype(np.float32)
        combo = np.array(combo).astype(np.float32)
        min = np.min(values)
        overallMin = np.min([overallMin, min])
        max = np.max(values)
        overallMax = np.max([overallMax, max])
        overallAverage += np.average(values)
        
        fig, ax = plt.subplots()
        ax.plot(times, values)
        ax.set_title("Voltage over Time")
        ax.set_ylabel("Voltage (V)")
        ax.set_xlabel("Time (s)")
        fig.tight_layout()
        if min < -0.02:
            savePath = os.path.join(path, "Noise", file[:-4])
            plt.savefig(f'{savePath}.png')
            np.savetxt(f'{savePath}.csv', combo, delimiter=",")
        else:
            savePath = os.path.join(path, "Pulses", file[:-4])
            plt.savefig(f'{savePath}.png')
            np.savetxt(f'{savePath}.csv', combo, delimiter=",")
        plt.close()
    
    

overallAverage /= len(files)

print(f"Min value: {overallMin}     Max value: {overallMax}     Average Value: {overallAverage}")
