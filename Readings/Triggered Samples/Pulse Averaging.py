import csv
import os
import numpy as np
import matplotlib.pyplot as plt
from numpy.lib.function_base import average
import scipy.signal as signal

path = os.getcwd()
files = []
datasets = []
allValues = []
allTimes = []

for file in os.listdir(os.path.join(path, "Pulses")):
    if os.path.splitext(file)[-1].lower() == ".csv":
        files.append(file)


overallMax = 0
overallMin = 0
maxIds = []

for file in files:
    times = []
    values = []
    combo = []
    filepath = os.path.join(path, "Pulses", file)
    with open(filepath) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            times.append(row[0])
            values.append(row[1])
            combo.append([row[0], row[1]])
        times = np.array(times).astype(np.float32)
        values =  np.array(values).astype(np.float32)
        allValues.append(values)
        allTimes = times
        min = np.amin(values)
        max = np.amax(values)

        overallMin = np.min([overallMin, min])
        overallMax = np.max([overallMax, max])
        maxId = np.where(values == max)[0][0]
        maxIds.append(maxId)
        datasets.append([times[500:-500], values[(maxId+1000-20001):(-20001+maxId)]])

average = np.zeros_like(datasets[0][1])
for id in range(len(datasets)):
    for id2 in range(len(datasets[id][1])):
        average[id2] += datasets[id][1][id2]

average /= len(datasets[0][1])

fig, ax = plt.subplots()
ax.plot(datasets[0][0], average)
ax.set_title("Average Pulse")
ax.set_ylabel("Voltage (V)")
ax.set_xlabel("Time (s)")
fig.tight_layout()
savePath = os.path.join(path, "Output", "Average Pulse")
plt.savefig(f'{savePath}.png')
plt.close()

fig, ax = plt.subplots()
ax.plot(datasets[0][0][10000:30000], average[10000:30000])
ax.set_title("Average Pulse")
ax.set_ylabel("Voltage (V)")
ax.set_xlabel("Time (s)")
fig.tight_layout()
savePath = os.path.join(path, "Output", "Average Pulse Zoomed")
plt.savefig(f'{savePath}.png')
plt.close()

N  = 5    # Filter order
Wn = 0.001 # Cutoff frequency
B, A = signal.butter(N, Wn, output='ba')
for id in range(len(datasets)):
    smooth_data = signal.filtfilt(B, A, allValues[id])
    plt.plot(allTimes, smooth_data, label=f'Plot {id}')
plt.title("All Pulses")
plt.ylabel("Voltage (V)")
plt.xlabel("Time (s)")
plt.legend(bbox_to_anchor=(1, 0.5), loc='center left')

savePath = os.path.join(path, "Output", "All Pulses Zoomed and Smoothed")
plt.savefig(f'{savePath}.png', bbox_inches='tight')
plt.close()

noiseMax = np.max(average[:30000])
noiseMin = np.min(average[:30000])
averageMaxId = np.where(average == np.amax(average))[0][0]

testVal = 1
count = 0
while testVal > noiseMax:
    testVal = average[averageMaxId + count]
    count += 1
averagePulseEndId = count + averageMaxId

# print(averageMaxId)
# print(averagePulseEndId)

print(f"Max value: {np.max(average)}V     Min value: {np.min(average)}V       Noise Max: {noiseMax}V       Noise Min: {noiseMin}V")
print(f'Pulse Start: {datasets[id][0][averageMaxId]}s       Pulse End: {datasets[id][0][averagePulseEndId]}s        Pulse duration: {datasets[id][0][averagePulseEndId]-datasets[id][0][averageMaxId]}s')
